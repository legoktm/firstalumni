FIRST Alumni Website

This is an open project for the FIRST Alumni website which will be launching soon.  

It is a collaborative project, and all alum are welcome to contribute.  
We're always open to ideas!  For the sake of a timely launch, we ask that all collaboration run through Lea Fairbanks darunada@gmail.com

This is intended to be an open community for information gathering, communication, and alumni organization in the FIRST community.  And more: if you have an idea please share it!

WE LOVE FIRST!  ROBOTS RULE! WOOHOO!