<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>FIRST Alumni</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="FIRST Alumni gathering website" />
        <meta name="keywords" content="FIRST first robotics alumni college robot robots volunteer" />
        <meta name="robots" content="INDEX,FOLLOW" />
        <meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE" />
        <meta HTTP-EQUIV="EXPIRES" CONTENT="0" />
        <meta HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE" />
        <link rel="icon" href="<?= image_url('favicon.ico') ?>" type="image/x-icon" />
        <link rel="shortcut icon" href="<?= image_url('favicon.ico') ?>" type="image/x-icon" />

        <script type="text/javascript">
            var BASE_URL = '<?= site_url() ?>';
            var IMG_PATH = '<?= image_url() ?>';
        </script>

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.js"></script>

        <?php
        
        $this->carabiner->js('bootstrap.min.js');
        $this->carabiner->css('bootstrap.min.css');
        
        $this->carabiner->display();
        ?>
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                
                
                
            </div>
            <div id="content">
                
            
            
            
            
          